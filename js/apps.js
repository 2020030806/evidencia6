let busqueda="";
function ajax() {
    const url = "https://jsonplaceholder.typicode.com/users";
    busqueda = document.getElementById('busqueda').value;
    if(busqueda == "" || busqueda >10 || busqueda < 1){
        alert("El valor de la busqueda no es valido")
      }else{

  
    axios
      .get(url)
      .then((res) => {
        mostrar(res.data);
      })
      .catch((err) => {
        console.error("Ha ocurrido un error: ", err);
      });
    }
  }
  
  function getUsers() {
    fetch("https://jsonplaceholder.typicode.com/users")
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
      });
  }
  
  function mostrar(data) {
    const res = document.getElementById("respuesta");
    res.innerHTML = "";
  
    data.forEach((item) => {
        if(busqueda == item.id){
      res.innerHTML = `<div class="caja">`+
          "<h5>ID: " + item.id + "</h5>" +
          "<h5>Nombre:" + item.name + "</h5>" +
          "<h5>Correo: " + item.email + "</h5>" +
          "<hr>" +
          "<h5>-- Domicilio -- </h5>" +
          "<hr>" +
          "<h5>           -Calle: " + item.address.street + "</h5>" +
          "<h5>           -Numero: " + item.address.suite+ "</h5>" +
          "<h5>           -Ciudad: " + item.address.city + "</h5>" +
      
        "</div>" +
        "<br>";
    }else{

    }
    });
  }
  
  
  function Limpiar(){
    const res = document.getElementById("respuesta");
    document.getElementById('busqueda').value = "";
    busqueda="";

  }

